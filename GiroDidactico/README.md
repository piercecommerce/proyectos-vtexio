# Table of Contents

- [Table of Contents](#table-of-contents)
  - [Enviroment](#enviroment)
  - [Repositories](#repositories)
    - [Store](#store)
    - [Custom Apps](#custom-apps)
      - [Lista de Imágenes](#lista-de-imágenes)
      - [Modal de Eventos](#modal-de-eventos)
      - [Mundo Giro](#mundo-giro)
      - [Medios de Pago](#medios-de-pago)
      - [Medios de Pago Productos](#medios-de-pago-productos)
      - [Comentario Pierce Commerce](#comentario-pierce-commerce)
    - [Envio Gratis](#envio-gratis)
  - [Implementations and Apps](#implementations-and-apps)
  - [Transactionals Mails](#transactionals-mails)
  - [Menu](#menu)


## Enviroment

[Store](https://girodidacticoar.myvtex.com/)

[Administrator](https://girodidacticoar.myvtex.com/admin)


## Repositories

List of repositories

### Store

[Repository](https://gitlab.com/piercecommerce/giro)

Git Clone: git@gitlab.com:piercecommerce/giro.git

### Custom Apps

[Repository](https://gitlab.com/piercecommerce/piercecommerce.giro)

Git Clone: git@gitlab.com:piercecommerce/piercecommerce.giro.git

#### Lista de Imágenes

Lista de Imágenes que se utiliza en la Landing de Medios de Pago.

Propiedades:
- Imagen
- Alt

#### Modal de Eventos

Modal para eventos Hotsale y Cybermonday.

Propiedades:
- Imagen
- Título
- Texto
- Color (Color de Fondo)
- Evento (Nombre del Evento para guardar en master data)
- Activar (Permite activar y desactivar el modal)

La información se guarda en la entidad Eventos, con los siguientes datos:
- Nombre
- Email
- Evento

#### Mundo Giro

Aplicación para armar los filtros y artículos en la Landing Mundo Giro.

Propiedades:
- Lista de Filtros:
  - Nombre
  - ID Filtro
- Lista de Artículos:
  - Imagen
  - Título
  - Descripción
  - Link
  - ID Filtro
- Artículos por Página

#### Medios de Pago

Lista de Promociones que se utiliza en la Landing de Medios de Pago.

Propiedades:
- Lista de Promociones:
  - Imagen
  - Días
  - Texto:
    - Hasta
    - Porcentaje
    - Reintegro
    - Cuotas
  - Vigencia (Modal)
  - Texto Vigencia (Modal)
  - Legales (Modal)
  - Locales Adheridos (Modal)

#### Medios de Pago Productos

Lista de Promociones que se muestran en la página del producto. Se configuran de acuerdo al día de la semana.

Propiedades:
- Lista de Promociones (Todos los días, Lunes, Martes, Miércoles, Jueves, Viernes, Sábados y Domingos):
  - Imagen
  - Título
  - Texto

#### Comentario Pierce Commerce

Agrega como comentario en el html la firma de Pierce Commerce


### Envio Gratis
[Repository](https://gitlab.com/piercecommerce/giro.minicart)

Git Clone: git@gitlab.com:piercecommerce/giro.minicart.git

Extensión de la app vtex.minicart de VTEX. Muestra en el minicart cuanto falta para tener envío gratis (Actualmente el envío gratis $8.000)


## Implementations and Apps

- Availability Notify
- Checkout UI Custom
  

## Transactionals Mails

- Access Key
- Order Confirmation
- Payment Approved
- Order Invoiced
- Order Shipped
- Order Delivered
- Back in Stock

## Menu

Utiliza el menú nativo de VTEX.
