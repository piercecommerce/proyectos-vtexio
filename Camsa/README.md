# Table of Contents

- [Table of Contents](#table-of-contents)
  - [Enviroment](#enviroment)
  - [Repositories](#repositories)
    - [Store](#store)
    - [Custom Apps](#custom-apps)
      - [Menu Desktop](#menu-desktop)
      - [Menu Mobile](#menu-mobile)
      - [CamCuotas](#camcuotas)
      - [Preguntas Frecuentes (Mobile)](#preguntas-frecuentes-mobile)
      - [Add To Cart Button Modal](#add-to-cart-button-modal)
      - [Comentario Pierce Commerce](#comentario-pierce-commerce)
  - [Implementations and Apps](#implementations-and-apps)
  - [Transactionals Mails](#transactionals-mails)


## Enviroment

[Store](https://camsabo.myvtex.com/)

[Administrator](https://camsabo.myvtex.com/admin)


## Repositories

List of repositories

### Store

[Repository](https://gitlab.com/piercecommerce/camsa)

Git Clone: git@gitlab.com:piercecommerce/camsa.git

### Custom Apps

[Repository](https://gitlab.com/piercecommerce/piercecommerce.camsa)

Git Clone: git@gitlab.com:piercecommerce/piercecommerce.camsa.git

#### Menu Desktop

Menú desktop, automático.

- Permite crear un menú de 3 niveles (departamento, categoría y subcategoría) totalmente automático.

#### Menu Mobile

Menú mobile, automático.

- Permite crear un menú de 3 niveles (departamento, categoría y subcategoría), estilo acordion, totalmente automático.

#### CamCuotas

Muestra el valor de CamCuotas y el valor inicial. Además muestra los precios desde Vtex.

### Preguntas Frecuentes (Mobile)

Permite el filtrado de preguntas frecuentes en Mobile.

Para poder lograr el filtrado el label del elemento tab-list.item debe ser:

- MOSTRAR TODO
- BANCOS
- DEVOLUCIÓN
- FACTURACIÓN
- OPERADOR
- PAGOS
- PRODUCTOS
- SEGURIDAD
- TIENDA ONLINE

Permite tener hasta 22 preguntas

### Add To Cart Button Modal

Muestra un modal con dos botones (Finalizar compra y Seguir buscando) luego de que el producto se agregó al carrito en la página del producto.

#### Comentario Pierce Commerce

Agrega como comentario en el html la firma de Pierce Commerce


## Implementations and Apps

- Abandoned Cart
- Checkout UI Custom


## Transactionals Mails

- Access Key
- Order Confirmation
- Payment Approved
- Order Cancelled
