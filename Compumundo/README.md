# Table of Contents

- [Table of Contents](#table-of-contents)
  - [Enviroment](#enviroment)
  - [Repositories](#repositories)
    - [Store](#store)
    - [Custom Apps](#custom-apps)
      - [Brand Name SEO](#brand-name-seo)
      - [Product Description SEO](#product-description-seo)
      - [Product Name SEO](#product-name-seo)
      - [Generic Element](#generic-element)
      - [Text SEO](#text-seo)
      - [Quick Menu Desktop](#quick-menu-desktop)
      - [Quick Menu Mobile](#quick-menu-mobile)
      - [Product Payments](#product-payments)
      - [Product Seller](#product-seller)
      - [Structured Data](#structured-data)
      - [Comentario Pierce Commerce](#comentario-pierce-commerce)
    - [Search Result](#search-results)
  - [Implementations and Apps](#implementations-and-apps)
  - [Transactionals Mails](#transactionals-mails)


## Enviroment

[Store](https://compumundoar.myvtex.com/)

[Administrator](https://compumundoar.myvtex.com/admin)


## Repositories

List of repositories

### Store

[Repository](https://gitlab.com/piercecommerce/compumundo)

Git Clone: git@gitlab.com:piercecommerce/compumundo.git

### Custom Apps

[Repository](https://gitlab.com/piercecommerce/piercecommerce.compumundo)

Git Clone: git@gitlab.com:piercecommerce/piercecommerce.compumundo.git

#### Brand Name SEO

Arma la marca en un tag h2 en la tarjeta del producto y en la página del producto

#### Generic Element

Genera un elemento genérico de acuerdo a sus propiedades

Propiedades:
- Tag: El tag html que se quiere generar
- Texto: El texto que va a tener
- Link: En el caso de tener link lo arma sino solo es texto

#### Product Description SEO

Arma la descripción del producto en un tag article en la página del producto

#### Product Name SEO

Arma la marca en un tag h1 en la tarjeta del producto y en la página del producto

#### Product Payments

Lista de Promociones que se muestran en la página del producto. Se configuran de acuerdo al día de la semana

Propiedades:
- Lista de Promociones (Todos los días, Lunes, Martes, Miércoles, Jueves, Viernes, Sábados y Domingos):
  - Imagen
  - Título
  - Texto

#### Product Seller

Muestra el seller al que pertenece el producto en la página del producto

#### Quick Menu Desktop

Menú desktop, totalmente personalizable.

Propiedades:
- Nombre Departamento
- Link Departamento
- Lista de Categorías
  - Nombre Categoría
  - Link Categoría
  - Lista Subcategorías
    - Nombre Subategoría
    - Link subategoría

- Permite crear un menú de 3 niveles (departamento, categoría y subcategoría)
- Debe tener como máximo 8 departamentos, ya que el menú es horizontal (Va a depender de la cantidad de caracteres que posea el nombre del departamento)
- En el caso de que tenga muchas categorías/subcategorías muestra un scroll

#### Quick Menu Mobile

Menú mobile, totalmente personalizable.

Propiedades:
- Nombre Departamento
- Link Departamento
- Lista de Categorías
  - Nombre Categoría
  - Link Categoría
  - Lista Subcategorías
    - Nombre Subategoría
    - Link subategoría

- Permite crear un menú de 3 niveles (departamento, categoría y subcategoría), estilo acordion

#### Structured Data

Inyecta un script en el head del HTML con la información del producto y sus reseñas/calificaciones

### Text SEO

Muestra un texto en un departamento, categoría o subcategoría (Debajo de los filtros)

#### Comentario Pierce Commerce

Agrega como comentario en el html la firma de Pierce Commerce


### Search Result
[Repository](https://gitlab.com/piercecommerce/compumundo.search-result)

Git Clone: git@gitlab.com:piercecommerce/compumundo.search-result.git

Extensión de la app vtex.search-result de VTEX para cambiar el tag h1 de la búsqueda a h2

## Implementations and Apps

- Reviews and Ratings
- Checkout UI Custom


## Transactionals Mails

- Access Key
- Order Confirmation
- Payment Approved
- Payment Denied
- Order Invoiced
- Order Shipped
- Order Delivered
- Order Cancelled
- Shipping Update
- Order Cancel Customer
- Cancellation
