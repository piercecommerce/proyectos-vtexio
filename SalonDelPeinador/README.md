# Table of Contents

- [Table of Contents](#table-of-contents)
  - [Enviroment](#enviroment)
  - [Repositories](#repositories)
    - [Store](#store)
    - [Custom Apps](#custom-apps)
      - [Preguntas Frecuentes (Mobile)](#preguntas-frecuentes-mobile)
      - [Banner Marca](#banner-marca)
      - [Ocultar menú contextual](#ocultar-menú-contextual)
      - [Preheader](#preheader)
      - [Especificación del Producto](#especificación-del-producto)
      - [Comentario Pierce Commerce](#comentario-pierce-commerce)
      - [Banner Marca y Colección](#banner-marca-y-coleccion)
      - [Cuotas sin intereses](#cuotas-sin-intereses)
      - [Cálculo de envío](#calculo-de-envio)
    - [Store Locator](#store-locator)
    - [Slider Layout](#slider-layout)
  - [Implementations and Apps](#implementations-and-apps)
  - [Transactionals Mails](#transactionals-mails)
  - [Menu](#menu)


## Enviroment

[Store](https://salondepeinadorar.myvtex.com/)

[Administrator](https://salondepeinadorar.myvtex.com/admin)


## Repositories

List of repositories

### Store

[Repository](https://gitlab.com/piercecommerce/salon-del-peinador)

Git Clone: git@gitlab.com:piercecommerce/salon-del-peinador.git

### Custom Apps

[Repository](https://gitlab.com/piercecommerce/piercecommerce.salon-del-peinador)

Git Clone: git@gitlab.com:piercecommerce/piercecommerce.salon-del-peinador.git

#### Preguntas Frecuentes (Mobile)

Permite el filtrado de preguntas frecuentes en Mobile.

Para poder lograr el filtrado el label del elemento tab-list.item debe ser:

- MOSTRAR TODO
- BANCOS
- DEVOLUCIÓN
- FACTURACIÓN
- OPERADOR
- PAGOS
- PRODUCTOS
- SEGURIDAD
- TIENDA ONLINE

Permite tener hasta 22 preguntas

#### Banner Marca

Muestra el banner de la marca en un contexto de búsqueda si cumple con las siguientes condiciones:

- El término buscado es igual a una marca (que se encuentre activa)
- Accediendo desde una url de marca

#### Ocultar menú contextual

Oculta el menú contextual del botón derecho del mouse para que no puedan inspeccionar el código.

No aplica para cart y/o checkout

#### Preheader

Muestra un preencabezado con mensajes e íconos personalizados, con animación desde abajo hacia arriba

#### Especificación del Producto

En el caso de tener especificaciones en el Producto muestra el título, sino no lo muestra.

#### Comentario Pierce Commerce

Agrega como comentario en el html la firma de Pierce Commerce

#### Banner Marca y Colección

Muestra el banner de la marca o colección en un contexto de búsqueda, en el caso de la marca lo muestra, si cumple con las siguientes condiciones:

- El término buscado es igual a una marca (que se encuentre activa)
- Accediendo desde una url de marca

#### Cuotas sin intereses

Muestra el mayor valor de la cuota sin interés en la tarjeta de producto y la página de producto

#### Cálculo de envío

Calcula el envío de acuerdo al código postal (Envío a domicilio / Retiro en tienda)

### Store Locator
[Repository](https://gitlab.com/piercecommerce/salon-del-peinador.store-locator)

Git Clone: git@gitlab.com:piercecommerce/salon-del-peinador.store-locator.git

Extensión de la app vtex.store-locator de VTEX. Toma la información de las sucursales de Master Data (Sucursales) para mostrarlas en la pantalla.

Campos:
- Dirección
- Nombre
- Imagen
- Latitud
- Longitud
- Localidad
- Retiro en Tienda
- Teléfono

### Slider Layout
[Repository](https://gitlab.com/piercecommerce/piercecommerce.slider-layout)

Git Clone: git@gitlab.com:piercecommerce/piercecommerce.slider-layout.git

Extensión de la app vtex.slider-layout de VTEX para corregir el Bug de la propiedad infinite en Mobile


## Implementations and Apps

- Carrito Abandonado
- Store Locator
- Checkout UI Custom


## Transactionals Mails

- Access Key
- Order Confirmation
- Payment Approved
- Payment Pending
- Order Shipped
- Abandoned Cart

## Menu

Utiliza el menú nativo de VTEX.

- Debe tener como máximo 8 departamentos, ya que el menú es horizontal (Va a depender de la cantidad de caracteres que posea el nombre del departamento)
- Cada departamento muestra hasta 10 categorías
- Cada categoría muestra hasta 4 subcategorías (Muestra 4 si tiene 4 y si tiene más se agrega el Ver todos)
